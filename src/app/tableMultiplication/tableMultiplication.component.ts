import { Component, Output, EventEmitter, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-tableMultiplication',
  templateUrl: './tableMultiplication.component.html',
  styleUrls: ['./tableMultiplication.component.css']
})

export class TableMultiplicationComponent implements OnInit {

  multForm!: FormGroup;
  isSubmitted = false;
  badMultNum = false;
  multTab = [1,2,3,4,5,6,7,8,9,10];
  constructor() { }
  @Output() laMult = new EventEmitter<number>();
  @Input() multNum!: number;

  ngOnInit(): void {
    this.multForm = new FormGroup({
      multNum: new FormControl(''),
    });

  }

  get formControls() { return this.multForm.controls; }

  submit() {
    this.isSubmitted = true;
    console.log("multNum :" + this.multForm.value.multNum);
    if (this.multForm.value.multNum == 0) {
      this.badMultNum = true;
      return;
    }else {
      this.laMult.emit(this.multForm.value.multNum);
      } 

  }


}
