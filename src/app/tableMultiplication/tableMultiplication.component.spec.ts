import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiplicationNumberComponent } from './tableMultiplication.component';

describe('MultiplicationNumberComponent', () => {
  let component: MultiplicationNumberComponent;
  let fixture: ComponentFixture<MultiplicationNumberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultiplicationNumberComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiplicationNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
