import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { TableMultiplicationComponent } from './tableMultiplication/tableMultiplication.component';
import { TablesMultiplicationComponent } from './tablesMultiplication/tablesMultiplications.component';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    TableMultiplicationComponent,
    TablesMultiplicationComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
