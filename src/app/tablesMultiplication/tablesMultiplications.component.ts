import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-tablesMultiplication',
  templateUrl: './tablesMultiplication.component.html',
  styleUrls: ['./tablesMultiplication.component.css']
})
export class TablesMultiplicationComponent implements OnInit {

  tableMultForm!: FormGroup;
  isSubmitted = false;
  badTableMultNum = false;
  tab = [1,2,3,4,5,6,7,8,9,10];
  tabchiffre = Array();
  @Output() leTableMultNum = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
    this.tableMultForm = new FormGroup({
      tableMultNum: new FormControl(''),
    });
  }

  get formControls() { return this.tableMultForm.controls; }

  submit2(){
    this.tabchiffre=[];
      for(let i = 1; i<=this.tableMultForm.value.tableMultNum; i++){
        this.tabchiffre.push(i)
      }
      console.table(this.tabchiffre);
  }


}
