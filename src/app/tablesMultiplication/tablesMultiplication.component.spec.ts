import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablesMultiplicationFormComponent } from './tablesMultiplications.component';

describe('TablesMultiplicationFormComponent', () => {
  let component: TablesMultiplicationFormComponent;
  let fixture: ComponentFixture<TablesMultiplicationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablesMultiplicationFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablesMultiplicationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
