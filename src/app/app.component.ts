import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'mulitiplication';
  multNum: number = 0;
  tableMultNum: number = 10;
  onMultAdded(event: number) {
    this.multNum = event;
  }

  getMult() {
    return this.multNum != 0;
  }



  /* // Déclaration de la variable (typée !) pour l'utilisateur
  nom:string = '';
  // Fonction retournant un booléen et non le nom
  getConnect() {
  return this.nom != '';
  }*/

}
